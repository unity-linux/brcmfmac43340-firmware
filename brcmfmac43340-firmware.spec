Summary:	Firmware for brcmfmac43340 SDIO WLAN Based Cards
Name:		brcmfmac43340-firmware
Version:	1.0
Release:	1%{?dist}
License:	Public
Group:		System Environment/Kernel
URL:		https://github.com/Asus-T100/firmware/tree/master/brcm

Source0: https://github.com/Asus-T100/firmware/raw/master/brcm/BCM4324B3.hcd
Source1: https://github.com/Asus-T100/firmware/raw/master/brcm/BCM43341B0.hcd
Source2: https://github.com/Asus-T100/firmware/raw/master/brcm/brcmfmac43241b4-sdio.txt
Source3: https://github.com/Asus-T100/firmware/raw/master/brcm/brcmfmac43340-sdio.txt

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build

BuildArch: noarch

%description
This package provides the firmware required for using
brcmfmac43340 802.11a/b/g SDIO WLAN based network adapters
and bcm43341B0 Bluetooth adapters.

%prep
# nothing to prep

%build
# nothing to build

%install
%{__mkdir_p} %{buildroot}/lib/firmware/brcm/
%{__install} -p -m 0644 ../BCM*.hcd %{buildroot}/lib/firmware/brcm/
%{__install} -p -m 0644 ../brcm*.txt %{buildroot}/lib/firmware/brcm/

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
/lib/firmware/brcm/*

%changelog
* Thu Jan 31 2019 JMiahMan <JMiahMan@unity-linux.org> - 1.0-1
- Initial Build for Unity-Linux
